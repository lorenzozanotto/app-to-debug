//
//  DraftEditorViewController.swift
//  Blueberry
//
//  Created by Lorenzo Zanotto on 29/03/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import UIKit
import CoreData

class DraftEditorViewController: ViewController {
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var contentField: UITextView!
    
    // The receiver drafts array
    var draft: Draft!
    var firstTimeCreated = false

    override func viewDidLoad() {
        super.viewDidLoad()

        configureEditorMode()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureEditorMode() {
        
        if (draft != nil) {
            firstTimeCreated = false
            self.titleField.text = draft.title
            self.contentField.text = draft.content
        } else {
            firstTimeCreated = true
        }
        
    }

    @IBAction func saveDraft(sender: AnyObject) {
        
        saveDraft()
        
    }
    
    
    func saveDraft() {
        
        if let managedObjectContext = (UIApplication.sharedApplication().delegate as? AppDelegate)?.managedObjectContext {
            draft = NSEntityDescription.insertNewObjectForEntityForName("Draft", inManagedObjectContext: managedObjectContext) as! Draft
            draft.title = self.titleField.text!
            draft.content = self.contentField.text
            
            do {
                try managedObjectContext.save()
                print("Saved \(draft.title)")
            } catch {
                print(error)
                return
            }
            
        }
        
    }
    

}








