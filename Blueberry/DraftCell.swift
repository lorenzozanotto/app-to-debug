//
//  DraftCell.swift
//  Blueberry
//
//  Created by Lorenzo Zanotto on 29/03/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import UIKit

class DraftCell: UITableViewCell {
    
    /*
     *  Defining the outlets that needs to be contained inside a Draft Cell
     */
    @IBOutlet weak var draftTitleLabel: UILabel!
    @IBOutlet weak var draftShortDescLabel: UILabel!
    
}