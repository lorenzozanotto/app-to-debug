//
//  DraftManager.swift
//  Blueberry
//
//  Created by Lorenzo Zanotto on 29/03/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation

class DraftManager {
    
    class var sharedInstance: DraftManager {
        struct Static {
            static let instance = DraftManager()
        }
        return Static.instance
    }
    
    var draftsCollection = [Draft]()
    
}